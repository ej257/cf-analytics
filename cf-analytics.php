<?php
/**
 * @link              http://cfgeoplugin.com/
 * @since             1.0.0
 * @package           CF_Analytics
 *
 * @wordpress-plugin
 * Plugin Name:       CF Wordpress Analytics
 * Plugin URI:        http://cfgeoplugin.com/
 * Description:       //
 * Version:           1.0.0
 * Author:            Goran Zivkovic
 * Author URI:        https://profiles.wordpress.org/ej207
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cf-analytics
 * Domain Path:       /languages
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// If someone try to called this file directly via URL, abort.
if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Plugin name
if( !defined( 'CFA_NAME' ) ) define( 'CFA_NAME', 'cf-analytics' );
// CF Geoplugin website
if( !defined( 'CFA_GEO_URL' ) ) define( 'CFA_GEO_URL', 'https://creativform.com' );

// Main plugin file
if( !defined( 'CFA_FILE' ) ) define( 'CFA_FILE', __FILE__ );
// Plugin root directory
if( !defined( 'CFA_ROOT' ) ) define( 'CFA_ROOT', rtrim( plugin_dir_path( CFA_FILE ), '/' ) );
// Inludes directory
if( !defined( 'CFA_INCLUDES' ) ) define( 'CFA_INCLUDES', CFA_ROOT . '/includes' );
// Current plugin version
if( !defined( 'CFA_VERSION' ) ) define( 'CFA_VERSION', '1.0.0' );
// Admin directory
if( !defined( 'CFA_ADMIN' ) ) define( 'CFA_ADMIN', CFA_ROOT . '/admin' );
// Plugin URL root
if( !defined( 'CFA_URL' ) ) define( 'CFA_URL', rtrim( plugin_dir_url( CFA_FILE ), '/' ) );
// Assets URL
if( !defined( 'CFA_ASSETS' ) ) define( 'CFA_ASSETS', CFA_URL . '/assets' );
// Plugin session prefix (controlled by version)
if ( !defined( 'CFA_PREFIX' ) ) define( 'CFA_PREFIX', 'cfa_'.preg_replace("/[^0-9]/Ui",'',CFA_VERSION).'_');


// Session control
function CFA_Session_Control()
{
    /**
     * Start sessions if not exists
     *
     * @author     Ivijan-Stefan Stipic <creativform@gmail.com>
     */
    if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
        if(function_exists('session_status') && session_status() == PHP_SESSION_NONE) {
            session_start(array(
                'cache_limiter' => 'private_no_expire',
                'read_and_close' => false,
            ));
        }
    }
    else if (version_compare(PHP_VERSION, '5.4.0') >= 0)
    {
        if (function_exists('session_status') && session_status() == PHP_SESSION_NONE) {
            session_cache_limiter('private_no_expire');
            session_start();
        }
    }
    else
    {
        if(session_id() == '') {
            if(version_compare(PHP_VERSION, '4.0.0') >= 0){
                session_cache_limiter('private_no_expire');
            }
            session_start();
        }
    }
}

// Call session control function
CFA_Session_Control();

// Include hook class
include CFA_INCLUDES . '/class-cf-analytics-global.php';
// Main class
if( file_exists( CFA_INCLUDES . '/class-cf-analytics.php' ) )
{
    include CFA_INCLUDES . '/class-cf-analytics.php';
}

// Check for CF Geoplugin
add_action( 'plugins_loaded', 'check_cf_geoplugin' );

register_activation_hook( CFA_FILE, array( 'CF_Analytics_Init', 'activate' ) );

// Check for CF Geoplugin installation
function check_cf_geoplugin()
{
    if( class_exists( 'CF_Geoplugin_Global' ) )
    {
        if( class_exists( 'CF_Analytics_Global' ) )
        {
            // Load hook class
            $hook = new CF_Analytics_Global;
            
            if( class_exists( 'CF_Analytics_Init' ) )
            {
                class CF_Analytics_Load extends CF_Analytics_Init
                {
                    function __construct()
                    {
                        $this->register_deactivation_hook( CFA_FILE, 'deactivate' );
                        $this->run();
                    }
                }
            }

            // Clear memory and cpu
            $hook = NULL;
        }

        // When everythin is ready load plugin
        function CF_Analytics()
        {
            if( class_exists( 'CF_Analytics_Load' ) ) return new CF_Analytics_Load;
        }

        // Lets start the show
        CF_Analytics();
    }
    else
    {
        add_action( 'admin_notices', 'cfgeo_notice' );
    }
}

// If not installed or not active CF Geoplugin
function cfgeo_notice()
{
    $message = sprintf( __(
        '<strong>CF Analytics requires CF Geoplugin to be installed and active. You can download
        <a href="%s" target="_blank">CF Geoplugin</a> here.</strong>', CFA_NAME), self_admin_url('plugin-install.php?s=cf+geoplugin&tab=search&type=term') );

    printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message ); 
}
<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Plugin Class
 *
 * @since      1.0.0
 * @package    CF_Analytics
 * @author     Goran Zivkovic
 */

if( !class_exists( 'CF_Analytics_Init' ) ):
class CF_Analytics_Init extends CF_Analytics_Global
{
    // Run plugin functionality
    public function run()
    {
        // Include browser info class
        if( file_exists( CFA_INCLUDES . '/class-cf-analytics-browser.php' ) )
        {
            require_once CFA_INCLUDES . '/class-cf-analytics-browser.php';
            if( class_exists( 'CF_Analytics_Browser' ) )
            {
                $browser = new CF_Analytics_Browser();
                $browser_info = array(
                    'browser_name' => $browser->getBrowser(),
                    'platform'     => $browser->getPlatform(),
                    'version'      => $browser->getVersion(),
                    'is_robot'     => $browser->isRobot()
                );
                $GLOBALS['cfa_browser'] = $browser_info;
            }
        }
        // Include frontend functionality
        if( file_exists( CFA_INCLUDES . '/class-cf-analytics-data-collector.php' ) )
        {
            require_once CFA_INCLUDES . '/class-cf-analytics-data-collector.php';
            if( class_exists( 'CF_Analytics_Data_Collector' ) )
            {
                new CF_Analytics_Data_Collector;
            }
        }
    }

    // Activate plugin
    public static function activate()
    {
        global $wpdb;

        // Table names
        $table_ip = $wpdb->prefix . self::TABLES['ip'];
        $table_url = $wpdb->prefix . self::TABLES['url'];
        $table_url_rel = $wpdb->prefix . self::TABLES['url_rel'];
        $table_geo = $wpdb->prefix . self::TABLES['geo'];
        $table_provider = $wpdb->prefix . self::TABLES['provider'];
        $table_browser = $wpdb->prefix . self::TABLES['browser'];
        $table_browser_rel = $wpdb->prefix . self::TABLES['browser_rel'];

        // Charset
        $charset_collate = $wpdb->get_charset_collate();
        // SQL to create/update tables
        $sql = "
        CREATE TABLE {$table_ip} (
            `id` BIGINT(255) NOT NULL AUTO_INCREMENT,
            `ip` varchar(40) NOT NULL,
            `ip_version` INT(1) NOT NULL DEFAULT 0,
            `first_visit` int(10) NOT NULL,
            `last_visit` int(10) NOT NULL,
            `count` INT(255) NOT NULL DEFAULT 0,
            PRIMARY KEY  (`id`)
        ) ENGINE=InnoDB {$charset_collate};
        
        CREATE TABLE {$table_url} (
            `id` BIGINT(255) NOT NULL AUTO_INCREMENT,
            `url` varchar(255) NOT NULL,
            `post_id` INT(255) NOT NULL,
            `count` INT(255) NOT NULL DEFAULT 0,
            PRIMARY KEY  (`id`)
        ) ENGINE=InnoDB {$charset_collate};
        
        CREATE TABLE {$table_url_rel} (
            `ip_id` BIGINT(255) NOT NULL,
            `url_id` BIGINT(255) NOT NULL
        ) ENGINE=InnoDB {$charset_collate};
        
        CREATE TABLE {$table_geo} (
            `ip_id` BIGINT(255) NOT NULL,
            `continent` varchar(50) NOT NULL,
            `country` varchar(50) NOT NULL,
            `region` varchar(50) NOT NULL,
            `city` varchar(50) NOT NULL
        ) ENGINE=InnoDB {$charset_collate};
        
        CREATE TABLE {$table_provider} (
            `ip_id` BIGINT(255) NOT NULL,
            `dns` varchar(160) NOT NULL,
            `dns_provider` varchar(160) NOT NULL
        ) ENGINE=InnoDB {$charset_collate};
        
        CREATE TABLE {$table_browser} (
            `id` BIGINT(255) NOT NULL AUTO_INCREMENT,
            `browser` varchar(60) NOT NULL,
            `version` varchar(20) NOT NULL,
            `platform` varchar(40) NOT NULL,
            `date` int(10) NOT NULL,
            `count` INT(255) NOT NULL DEFAULT 0,
            PRIMARY KEY  (`id`)
        ) ENGINE=InnoDB {$charset_collate};
        
        CREATE TABLE {$table_browser_rel} (
            `ip_id` BIGINT(255) NOT NULL,
            `browser_id` BIGINT(255) NOT NULL
        ) ENGINE=InnoDB {$charset_collate};

        ALTER TABLE `{$table_url_rel}` ADD CONSTRAINT `{$table_url_rel}_fk0` FOREIGN KEY (`ip_id`) REFERENCES `{$table_ip}`(`id`) ON DELETE CASCADE  ON UPDATE CASCADE;
        
        ALTER TABLE `{$table_url_rel}` ADD CONSTRAINT `{$table_url_rel}_fk1` FOREIGN KEY (`url_id`) REFERENCES `{$table_url}`(`id`) ON DELETE CASCADE  ON UPDATE CASCADE;
        
        ALTER TABLE `{$table_geo}` ADD CONSTRAINT `{$table_geo}_fk0` FOREIGN KEY (`ip_id`) REFERENCES {$table_ip}(`id`) ON DELETE CASCADE  ON UPDATE CASCADE;
        
        ALTER TABLE `{$table_provider}` ADD CONSTRAINT `{$table_provider}_fk0` FOREIGN KEY (`ip_id`) REFERENCES `{$table_ip}`(`id`) ON DELETE CASCADE  ON UPDATE CASCADE;
        
        ALTER TABLE `{$table_browser_rel}` ADD CONSTRAINT `{$table_browser_rel}_fk0` FOREIGN KEY (`ip_id`) REFERENCES `{$table_ip}`(`id`) ON DELETE CASCADE  ON UPDATE CASCADE;
        
        ALTER TABLE `{$table_browser_rel}` ADD CONSTRAINT `{$table_browser_rel}_fk1` FOREIGN KEY (`browser_id`) REFERENCES `{$table_browser}`(`id`) ON DELETE CASCADE  ON UPDATE CASCADE;
        "; 

        // `

        // Require dbDelta to create/update table
		require_once ( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbdelta( $sql );
    }
}
endif;
<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Frontend part of plugin
 *
 * @since      1.0.0
 * @package    CF_Analytics
 * @author     Goran Zivkovic
 */
if( !class_exists( 'CF_Analytics_Data_Collector' ) ) :
class CF_Analytics_Data_Collector extends CF_Analytics_Global
{
    // CF Geoplugin GeoData
    protected $CFGEO = array();

    // Browser data
    protected $browser = array();

    function __construct()
    {
        // If cant access cfgeo global or if visitor is on admin page prevent actions
        if( !isset( $GLOBALS['CFGEO'] ) || !isset( $GLOBALS['cfa_browser'] ) || is_admin() ) return false;
        
        $this->browser = $GLOBALS['cfa_browser'];
        if( $this->browser['is_robot'] ) return false; // Dont count visits from crawlers/bots/etc...

        $this->CFGEO = $GLOBALS['CFGEO'];
        $this->add_action( 'wp', 'collect_visitor_data' ); // When WP object is loaded we have full control
    }

    // Collect all informations about visitor
    public function collect_visitor_data()
    {
        // If already checked ip dont do that again
        if( !isset( $_SESSION[CFA_PREFIX . 'session_ip'] ) || $_SESSION[CFA_PREFIX . 'session_ip'] != $this->CFGEO['ip'] ) 
        {
            // Check visitor by IP
            $this->check_ip_info();

            // Check visitor's browser
            $this->check_browser_info();

            // Connect visitor's ip and browser
            $this->relation_ip_browser();

            // Save visitor ip to session
            $this->set_session_ip( $this->CFGEO['ip'] );
        }

        // Check url that is visited
        $this->check_url_info();
    }

    // Check IP informations from DB 
    public function check_ip_info()
    {
        global $wpdb;
            
        $table_name = $wpdb->prefix . self::TABLES['ip'];
        $result = $wpdb->get_row( 
            $wpdb->prepare( 
                "SELECT * FROM {$table_name} WHERE ip = %s"
                , $this->CFGEO['ip']
            ), ARRAY_A
        );
        
        if( empty( $result ) ) $result = $this->insert_ip_info();

        return $result;
    }

    // Update info in ip table
    public function update_ip_info()
    {
        global $wpdb;
         
        $table_name = $wpdb->prefix . self::TABLES['ip'];
        $count = $this->check_ip_info();
        $id = (int)$count['id'];
        $count = (int)$count['count'];

        $wpdb->update(
            $table_name,
            array(
                'last_visit'    => time(),
                'count'         => ++$count
            ),
            array( 'id' => $id ),
            array(
                '%d',
                '%d'
            ),
            array( '%d' )
        );

    }

    // Insert info into ip table
    public function insert_ip_info() 
    {
        global $wpdb; 

        $table_ip = $wpdb->prefix . self::TABLES['ip'];
        $wpdb->insert(
            $table_ip,
            array(
                'ip'            => $this->CFGEO['ip'],
                'ip_version'    => $this->CFGEO['ip_version'],
                'first_visit'   => time(),
                'last_visit'    => time(),
                'count'         => 1
            ),
            array(
                '%s',
                '%d',
                '%d',
                '%d',
                '%d'
            )
        );

        // Connect visitor geo data with ip
        $data = $this->check_ip_info();
        $ip_id = (int)$data['id'];

        $table_geo = $wpdb->prefix . self::TABLES['geo'];
        if( !empty( $this->CFGEO['continent'] ) || !empty( $this->CFGEO['country'] ) || !empty( $this->CFGEO['region'] ) || !empty( $this->CFGEO['city'] ) ) 
        {
            $wpdb->insert(
                $table_geo,
                array(
                    'ip_id'         => $ip_id,
                    'continent'     => $this->CFGEO['continent'],
                    'country'       => $this->CFGEO['country'],
                    'region'        => $this->CFGEO['region'],
                    'city'          => $this->CFGEO['city']
                ),
                array(
                    '%d',
                    '%s',
                    '%s',
                    '%s',
                    '%s'
                )
            );
        }

        // Connect visitor ip and provider
        if( !empty( $this->CFGEO['ip_dns'] ) || !empty( $this->CFGEO['ip_dns_provider'] ) )
        {
            $table_provider = $wpdb->prefix . self::TABLES['provider'];
            $wpdb->insert(
                $table_provider,
                array(
                    'ip_id'         => $ip_id,
                    'dns'           => $this->CFGEO['ip_dns'],
                    'dns_provider'  => $this->CFGEO['ip_dns_provider']
                ),
                array(
                    '%d',
                    '%s',
                    '%s'
                )
            );
        }

        return $data;
    }

    // Check informations about visitor browser
    public function check_browser_info()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::TABLES['browser'];
        $result = $wpdb->get_row( 
            $wpdb->prepare( 
                "SELECT * FROM {$table_name} WHERE browser = %s AND version = %s AND platform = %s"
                ,$this->browser['browser_name'], $this->browser['version'], $this->browser['platform']
            ), ARRAY_A
        );

        if( empty( $result ) ) $result = $this->insert_browser_info();
    
        return $result;
    }

    // Update browser info
    public function update_browser_info()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::TABLES['browser'];
        $data = $this->check_browser_info();
        $count = (int)$data['count'];
        $id = (int)$data['id'];

        $wpdb->update(
            $table_name,
            array(
                'date'          => time(),
                'count'         => ++$count
            ),
            array( 'id' => $id ),
            array(
                '%d',
                '%d'
            ),
            array( '%d' )
        );
    }

    // Insert browser info
    public function insert_browser_info()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::TABLES['browser'];
        $wpdb->insert(
            $table_name,
            array(
                'browser'       => ucfirst( $this->browser['browser_name'] ), // unknown -> Unknown, if can't recognise browser name
                'version'       => ucfirst( $this->browser['version'] ), // unknown -> Unknown, if can't recognise browser version
                'platform'      => ucfirst( $this->browser['platform'] ), // unknown -> Unknown, if can't recognise browser platform
                'date'          => time(),
                'count'         => 1
            ),
            array(
                '%s',
                '%s',
                '%s',
                '%d',
                '%d'
            )
        );

        $data = $this->check_browser_info();

        return $data;
    }

    // Connect visitor's ip and browser
    public function relation_ip_browser()
    {
        global $wpdb;

        $table_ip = $wpdb->prefix . self::TABLES['ip'];
        $table_browser = $wpdb->prefix . self::TABLES['browser'];
        $table_relation = $wpdb->prefix . self::TABLES['browser_rel'];

        $browser_id = $this->check_browser_info();
        if( empty( $browser_id ) ) return false;
        $browser_id = (int)$browser_id['id'];

        $ip_id = $this->check_ip_info();
        if( empty( $ip_id ) ) return false;
        $ip_id = (int)$ip_id['id'];

        $result = (int)$wpdb->get_row(
            $wpdb->prepare(
                "SELECT * FROM {$table_relation} WHERE ip_id = %d AND browser_id = %d",
                $ip_id, $browser_id
            ), ARRAY_A
        );

        if( empty( $result ) )
        {
            $wpdb->insert(
                $table_relation,
                array(
                    'ip_id'         => $ip_id,
                    'browser_id'    => $browser_id
                ),
                array(
                    '%d',
                    '%d'
                )
            );
        }
    }

    // Check informations about URL
    public function check_url_info()
    {
        global $wpdb, $post;

        $table_name = $wpdb->prefix . self::TABLES['url'];

        $data = array(
            'post_id'   => $post->ID,
            'url'       => get_permalink($post)
        );

        $result = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT * FROM {$table_name} WHERE post_id = %d",
                $post->ID
            ), ARRAY_A
        );

        if( empty( $result ) ) 
        {
            $this->insert_url_info( $data );
            $this->relation_ip_url();
        }
        else $this->update_url_info( $result );
        
        $this->update_browser_info();
        $this->update_ip_info();
    }

    // Update URL info
    public function update_url_info( $data )
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::TABLES['url'];

        $count = (int)$data['count'];

        $wpdb->update(
            $table_name,
            array(
                'url'       => $data['url'],
                'count'     => ++$count
            ),
            array( 'post_id' => $data['post_id'] ),
            array(
                '%s',
                '%d'
            ),
            array( '%d' )
        ); 

    }

    // Insert URL info
    public function insert_url_info( $data )
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::TABLES['url'];

        $wpdb->insert(
            $table_name,
            array(
                'url'       => $data['url'],
                'post_id'   => $data['post_id'],
                'count'     => 1
            ),
            array(
                '%s',
                '%d',
                '%d'
            )
        );
    }

    // Connect visited url with visitor ip
    public function relation_ip_url()
    {
        global $wpdb;

        $table_ip = $wpdb->prefix . self::TABLES['ip'];
        $table_url = $wpdb->prefix . self::TABLES['url'];
        $table_relation = $wpdb->prefix . self::TABLES['url_rel'];

        $ip_id = $this->check_ip_info();
        if( empty( $ip_id ) ) return false;
        $ip_id = (int)$ip_id['id'];

        $url_id = $this->check_url_info();
        if( empty( $url_id ) ) return false;
        $url_id = (int)$url_id['id'];

        $wpdb->insert(
            $table_relation,
            array(
                'ip_id'    => $ip_id,
                'url_id'   => $url_id
            ),
            array(
                '%d',
                '%d'
            )
        );
    }
}
endif;

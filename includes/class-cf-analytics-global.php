<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Hooks, actions and other helpers
 *
 * @since      1.0.0
 * @package    CF_Analytics
 * @author     Goran Zivkovic
 */

if( !class_exists( 'CF_Analytics_Global' ) ) :
class CF_Analytics_Global
{
    // Table names
    const TABLES = array(
        'ip'            => 'cf_analytics_ip',
        'url'           => 'cf_analytics_url',
        'url_rel'       => 'cf_analytics_url_relation',
        'geo'           => 'cf_analytics_geolocation',
        'provider'      => 'cf_analytics_provider',
        'browser'       => 'cf_analytics_browser',
        'browser_rel'   => 'cf_analytics_browser_relation'
    );

    function __construct()
    {
        
    }

    // Hook for register_activation_hook()
    public function register_activation_hook( $file, $function )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );
        
        register_activation_hook( $file, $function );
    }

    // Hook for register_deactivation_hook()
    public function register_deactivation_hook( $file, $function )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );
        
        register_deactivation_hook( $file, $function );
    }

    // Hook for add_action()
    public function add_action( $hook, $function, $priority = 10, $accepted_args = 1 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );

        return add_action( (string)$hook, $function, (int)$priority, (int)$accepted_args );
    }

    // Hook for add_filter()
    public function add_filter( $hook, $function, $priority = 10, $accepted_args = 1 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );

        return add_filter( (string)$hook, $function, (int)$priority, (int)$accepted_args );
    }

    // Hook for remove_filter()
    public function remove_filter( $hook, $function, $priority = 10 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );

        return remove_filter( (string)$hook, $function, (int)$priority );
    }

    // Hook for remove_action()
    public function remove_action( $hook, $function, $priority = 10 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );
   
        return remove_action( (string)$hook, $function, (int)$priority );
    }

    // Set visitor ip into session
    public function set_session_ip( $ip )
    {
        if( isset( $_SESSION[CFA_PREFIX . 'session_ip'] ) ) unset( $_SESSION[CFA_PREFIX . 'session_ip'] ); // Just in case to be safe
        $_SESSION[CFA_PREFIX . 'session_ip'] = $ip;

        return $_SESSION[CFA_PREFIX . 'session_ip'];
    }

    /**
     * Get search engine that user used to search or visit site
     * 
     * name = The proper name of search engine
     * translated = The translated name of search engine to the local language
     * tag = One short word, lowercase that represents search engine
     * sqlpattern = Either a single SQL style search pattern OR an array or search patterns to match the hostname in a URL against 
     * regexpattern = Either a single regex style search pattern OR an array or search patterns to match the hostname in a URL against
     * querykey = The URL key that contains the search string for the search engine
     * image = the name of the image file to associate with this search engine ( just file name )
     */
    public function searchengine_list( $all = false )
    {
        $engines = array(
            'ask'        => array(
                'name'         => 'Ask.com',
                'translated'   => __( 'Ask.com', CFA_NAME ),
                'tag'          => 'ask',
                'sqlpattern'   => '%ask.com%',
                'regexpattern' => '/ask\.com/i',
                'querykey'     => 'q',
                'image'        => 'ask.png',
            ),
            'baidu'      => array(
                'name'         => 'Baidu',
                'translated'   => __( 'Baidu', CFA_NAME ),
                'tag'          => 'baidu',
                'sqlpattern'   => '%baidu.com%',
                'regexpattern' => '/baidu\.com/i',
                'querykey'     => 'wd',
                'image'        => 'baidu.png',
            ),
            'bing'       => array(
                'name'         => 'Bing',
                'translated'   => __( 'Bing', CFA_NAME ),
                'tag'          => 'bing',
                'sqlpattern'   => '%bing.com%',
                'regexpattern' => '/bing\.com/i',
                'querykey'     => 'q',
                'image'        => 'bing.png',
            ),
            'clearch'    => array(
                'name'         => 'clearch.org',
                'translated'   => __( 'clearch.org', CFA_NAME ),
                'tag'          => 'clearch',
                'sqlpattern'   => '%clearch.org%',
                'regexpattern' => '/clearch\.org/i',
                'querykey'     => 'q',
                'image'        => 'clearch.png',
            ),
            'duckduckgo' => array(
                'name'         => 'DuckDuckGo',
                'translated'   => __( 'DuckDuckGo', CFA_NAME ),
                'tag'          => 'duckduckgo',
                'sqlpattern'   => array( '%duckduckgo.com%', '%ddg.gg%' ),
                'regexpattern' => array( '/duckduckgo\.com/i', '/ddg\.gg/i' ),
                'querykey'     => 'q',
                'image'        => 'duckduckgo.png',
            ),
            'google'     => array(
                'name'         => 'Google',
                'translated'   => __( 'Google', CFA_NAME ),
                'tag'          => 'google',
                'sqlpattern'   => '%google.%',
                'regexpattern' => '/google\./i',
                'querykey'     => 'q',
                'image'        => 'google.png',
            ),
            'yahoo'      => array(
                'name'         => 'Yahoo!',
                'translated'   => __( 'Yahoo!', CFA_NAME ),
                'tag'          => 'yahoo',
                'sqlpattern'   => '%yahoo.com%',
                'regexpattern' => '/yahoo\.com/i',
                'querykey'     => 'p',
                'image'        => 'yahoo.png',
            ),
            'yandex'     => array(
                'name'         => 'Yandex',
                'translated'   => __( 'Yandex', CFA_NAME ),
                'tag'          => 'yandex',
                'sqlpattern'   => '%yandex.ru%',
                'regexpattern' => '/yandex\.ru/i',
                'querykey'     => 'text',
                'image'        => 'yandex.png',
            ),
        );

        // Later add option to select which search engine to disable

        return $engines;
    }

    // Get total number of posts/pages in WP
    public function count_posts( $post = 'page' )
    {
        $count = wp_count_posts( $page );

        if( is_object( $count ) ) return $count->publish; // Return number of published posts/pages only
    }

    // Get total number of comment in WP
    public function count_comments()
    {
        global $wpdb;

        $count = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->comments} WHERE comment_approved = '1'" );

        return $count;
    }

    // Get total number of spam comments IF askimet is installed
    public function count_spam_comments()
    {
        $spam = get_option( 'askimet_spam_count' );
        if( $spam === false ) return false;
        return number_format_i18n( $spam );
    }

    // Get total number of users in WP
    public function count_users()
    {
        $count = count_users();

        return $count['total_users'];
    }

    // Get search engine name
    public function get_search_engine_name( $all = false )
    {
        $all_engines = $this->searchengine_list( true );
        $name = '';
        $referer = $_SERVER['HTTP_REFERER'];

        if( empty( $all_engines ) || empty( $referer ) ) return $name;

        foreach( $all_engines as $engine => $info )
        {
            if( is_array( $info['regexpattern'] ) )
            {
                foreach( $info['regexpattern'] as $pattern )
                {
                    if( preg_match( $pattern, $referer ) !== false ) $name = $info['name'];
                    break;
                }
            }
            else
            {
                if( preg_match( $info['regexpattern'], $referer ) !== false ) $name = $info['name'];
                break;
            }
        }

        return $name;
    }

    // Get search keyword
    public function get_search_keywords()
    {
        $all_engines = $this->searchengine_list();
        $kw = '';
        $referer = parse_url( $_SERVER['HTTP_REFERER'] );

        var_dump( $referer );

        if( empty( $all_engines ) || empty( $referer ) ) return $kw;

        parse_str( $referer['query'], $referer );

        var_dump( $referer );
    }
}
endif;